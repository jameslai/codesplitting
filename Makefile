.PHONY: dev.setup dev.start prod.build

# Setup your local development environment
dev.setup:
	cd webserver; pip install -r requirements.txt
	cd src; npm install

# Start the development service
dev.start:
	FLASK_APP=webserver/server FLASK_ENV=development DEPLOYMENT=local VERSION=1.34.9 flask run --port=8888

# Build the production artifacts
prod.build:
	rm -rf build; cd src; npm run build
