import * as React from "react";
import { Link } from "react-router-dom";

export default function Worksheets() {
    return (
        <div>
            <Link to="/">Back Home</Link>
            <h1>Worksheets</h1>
        </div>
    )
}