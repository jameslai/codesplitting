import * as React from "react";
import { NavLink, Route, Routes } from "react-router-dom"
import { Home, Data, Worksheets, Settings } from "../loadables"

export default function Nav() {
    // Nav makes the decision not to render the navigation if we are at /worksheets
    return (
        <Routes>
            <Route path="/worksheets" element={<></>} />
            <Route path="*" element={
                <nav className="col-span-2">
                    <p>Navigation</p>
                    <NavLink to="/" onMouseOver={Home.preload}><div>Home</div></NavLink>
                    <NavLink to="/data" onMouseOver={Data.preload}><div>Data</div></NavLink>
                    <NavLink to="/worksheets" onMouseOver={Worksheets.preload}><div>Worksheets</div></NavLink>
                    <NavLink to="/settings" onMouseOver={Settings.preload}>{({ isActive }) => (
                        <div>
                            <span className={isActive ? "font-bold" : ""}>Settings</span>
                            {isActive ? <div className="ml-4">Admin</div> : ""}
                        </div>
                    )}</NavLink>
                </nav>
            } />
        </Routes>
    )
}