/**
 * App entrypoint. Kicks off our application
 */
import * as React from "react";
import { Routes, Route, NavLink } from "react-router-dom"
import { Data, Worksheets, Settings, Home } from "./loadables"
import Nav from "./components/Nav"

// Bundled
import Loading from "./components/Loading"

export default function App(snowflake) {
    return (
        <div className="grid grid-cols-12">
            <Nav />
            <div>
                <p>Content</p>
                <Routes>
                    <Route path="data" element={<React.Suspense fallback={<Loading />}><Data /></React.Suspense>}></Route>
                    <Route path="worksheets" element={<React.Suspense fallback={<Loading />}><Worksheets /></React.Suspense>}></Route>
                    <Route path="settings" element={<React.Suspense fallback={<Loading />}><Settings /></React.Suspense>}></Route>
                    <Route path="/" element={<React.Suspense fallback={<Loading />}><Home /></React.Suspense>}></Route>
                </Routes>
            </div>
        </div>
    )
}