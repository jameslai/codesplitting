
// We use loadable rather than React.lazy to achieve
// 1. No flash of our loading component when it isn't necessary
// 2. Preloading
import loadable from '@loadable/component'

// Load all entry points here
export const Data = loadable(() => import('./pages/Data'))
export const Worksheets = loadable(() => import("./pages/Worksheets"))
export const Settings = loadable(() => import("./pages/Settings"))
export const Home = loadable(() => import("./pages/Home"))