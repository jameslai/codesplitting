/*
 * This file is responsible for attaching our application to the DOM. This should
 * be the only file which is aware of the DOM and/or the window. Therefore, any
 * global values from the window object should be generically imported here.
 */
import * as React from "react";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom"

import App from "./App"

const root = createRoot(document.getElementById("root"));

root.render(
    <BrowserRouter>
        <App snowflake={(window as any).snowflake} />
    </BrowserRouter>
)