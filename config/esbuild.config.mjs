/**
 * Defines common/global esbuild configuration
 */
export default {
    entryPoints: [
        './src/snowflake.tsx',
    ],

    // Ensure hashes are added to our entrypoints
    entryNames: '[name]-[hash]',

    // Create a metadata entry so we can read hashes
    metafile: true,

    // Output directory
    outdir: 'build',
    
    // Bundle files
    bundle: true,

    // Enable code splitting
    splitting: true,

    // Required for code splitting
    format: "esm"
}