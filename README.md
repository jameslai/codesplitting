# Frontend Architectural POC

This POC implements/highlights a couple concepts.

### Serve the index/template file with a web service

Leaving the index.html file to be served statically is limiting in many ways. This PoC demonstrates using a light web service to serve the template.

### Include desired environment variables at page load time on the window

As our index.html page is served by our web service, we can easily expose environment variables on the window object (in a scoped object).

### Include information such as deployment on the scoped object

Rather than baking this information into the asset, this information can come naturally from the environment.

### Dynamically load major sections

Do not load everything all at once. Rather, load conditionally when the user expresses interest in a section.

### Preload on mouse over

Much like Next.js, preload components on mouse over, which will often make loading appear instantaneous.

### Load global objects at the top of the application

Components shouldn't be window-aware, load this information only in a single DOM-aware location.

### Keep the entrypoint simple

The entrypoint should be remarkably simple. Its responsibility should be limited to loading the application at the correct location in the DOM.

### Use a simple Makefile

Makefile is a universal tool. Use it.

### Namespace Makefile targets

Use "prod" and "dev" to separate concerns. "ci" is also a great choice.

### Antipatterns

* Should be running in a container utilizing Docker compose
* Should be running as close to production as possible, in this case having the web service serving static assets isn't ideal - put NGINX in front
* Utilize watchexec to monitor filesystem changes