import os
import json
from flask import Flask, render_template, send_from_directory

app = Flask(__name__)

cwd = os.path.dirname(os.path.abspath(__file__))
build_dir = os.path.join(cwd, "..", "build")

# Get environment variables
deployment = os.environ['DEPLOYMENT']
version = os.environ['VERSION']

# Load our entrypoints metadata
with open(os.path.join(build_dir, 'entrypoints.json')) as f:
    entrypoints = json.load(f)

@app.route("/js/<path:filename>")
def JS(filename):
    return send_from_directory(build_dir, filename)

@app.route("/", defaults={ "path": "" })
@app.route("/<string:path>")
@app.route("/<path:path>")
def Home(path):
    return render_template(
        "index.j2",
        entrypoints=entrypoints,
        deployment=deployment,
        version=version,
    )