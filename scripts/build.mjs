import * as esbuild from 'esbuild'
import fs from "node:fs"
import path from "node:path"
import config from '../config/esbuild.config.mjs'

const buildDir = "build/"
const ext = ".js"
const separator = "-"
const entryPointName = "snowflake"

let result = await esbuild.build({
    ...config
    // Extend config below
})

function extractFilenameFromEntryPointsConfig() {
    return config.entryPoints.map(ep => {
        return path.parse(ep).name
    })
}

// The output of the metadata includes all files written to the filesystem,
// which includes both entrypoints, common chunks as well as dynamically
// imported modules. Since we're largely only interested in the hash
// of our entrypoints, we need to look through the metadata for our named
// entrypoints
function buildEntryPointsHashmap(metafile) {
    const entryPoints = extractFilenameFromEntryPointsConfig()
    return Object.fromEntries(Object.keys(metafile.outputs).map(output => {
        return processMetadataOutput(output, entryPoints)
    }))
}

// Determines if an output is one of our entrypoints
function processMetadataOutput(output, entryPoints) {
    const filename = path.parse(output).name
    if(isEntryPoint(filename, entryPoints)) {
        // Very much assumes we're not using hyphens anywhere else in the filename
        const parts = filename.split(separator)
        return [parts[0], parts[1]]
    }
    return []
}

function isEntryPoint(filename, entryPoints) {
    return entryPoints.some(ep => {
        return filename.startsWith(ep)
    })
}

fs.writeFileSync(`${buildDir}/entrypoints.json`, JSON.stringify(buildEntryPointsHashmap(result.metafile)))
